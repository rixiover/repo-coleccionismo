import { Injectable } from '@angular/core';
import { InfoCartel } from '../interfaces/infoCartel';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CartelesService {
  _carteles: InfoCartel;
  constructor(private http: HttpClient) {}

  getCarteles() {
    return this.http.get('/assets/carteles.json');
  }
}

