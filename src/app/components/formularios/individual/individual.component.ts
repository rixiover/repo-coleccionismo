import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FirebaseService } from 'src/app/services/firebase.service';
import { FicheroImagen } from 'src/app/interfaces/imagen';

@Component({
  selector: 'app-individual',
  templateUrl: './individual.component.html',
  styleUrls: ['./individual.component.css']
})
export class IndividualComponent implements OnInit {
  public estaEncima = false;
  public formulario: FormGroup;

  public imagenes: FicheroImagen[] = [];
  constructor(private _service: FirebaseService) {
    this.formulario = new FormGroup({
      nombre: new FormControl('', Validators.required),
      grupo: new FormControl('', Validators.required),
      subgrupo: new FormControl('', Validators.required),
      fecha: new FormControl('', Validators.required),
      ilustrador: new FormControl('', Validators.required),
      tipoIlustracion: new FormControl('', Validators.required),
      valoracion: new FormControl('', Validators.required)
    });
  }

  guardarRegistro() {
    this._service.subirRegistroIndividual(this.formulario.value, this.imagenes);
  }

  ficherosSoltados(event) {
    if (this.imagenes.length > 0) {
      this._prevenirEvento(event);
      this.estaEncima = false;
      return;
    }
    this._prevenirEvento(event);
    this.estaEncima = false;
    console.log(event.dataTransfer.files);
    Array.from(event.dataTransfer.files)
      .filter((fichero: File) => {
        if (fichero.type.indexOf('image') === 0) {
          console.log('Todo en orden');
          return true;
        } else {
          console.log('Este fichero no es de tipo imagen', fichero.name);
          return false;
        }
      })
      .forEach((fichero: File) => {
        const imagen: FicheroImagen = {
          nombre: fichero.name.slice(0, -4),
          peso: fichero.size,
          fichero,
          progreso: 0
        };
        this.imagenes.push(imagen);
      });
    console.log(this.imagenes);
  }

  ficherosPorEncima(ev) {
    this._prevenirEvento(ev);
    this.estaEncima = true;
  }

  ficherosPorFuera(ev) {
    this._prevenirEvento(ev);
    this.estaEncima = false;
  }

  private _prevenirEvento(event) {
    event.preventDefault();
    event.stopPropagation();
  }

  ngOnInit() {
    window.addEventListener(
      'dragover',
      function(e) {
        e.preventDefault();
      },
      false
    );
    window.addEventListener(
      'drop',
      function(e) {
        e.preventDefault();
      },
      false
    );
  }
}
