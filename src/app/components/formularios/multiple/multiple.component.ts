import { Component, OnInit } from '@angular/core';
import { FicheroImagen } from 'src/app/interfaces/imagen';
import { CartelesService } from 'src/app/services/carteles.service';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-multiple',
  templateUrl: './multiple.component.html',
  styleUrls: ['./multiple.component.css']
})
export class MultipleComponent implements OnInit {
  public estaEncima = false;
  infoCarteles: any;
  public imagenes: FicheroImagen[] = [];
  constructor(
    private _infoCarteles: CartelesService,
    private _firebaseStorage: FirebaseService
  ) {}

  ngOnInit() {
    this.estaEncima = false;

    this.cargarInfoCarteles();

    window.addEventListener(
      'dragover',
      function(e) {
        e.preventDefault();
      },
      false
    );
    window.addEventListener(
      'drop',
      function(e) {
        e.preventDefault();
      },
      false
    );
  }

  cargarInfoCarteles() {
    this._infoCarteles.getCarteles().subscribe(res => {
      this.infoCarteles = res;
      console.log(res);
    });
  }

  ficherosPorEncima(ev) {
    this._prevenirEvento(ev);
    this.estaEncima = true;
  }

  ficherosPorFuera(ev) {
    this._prevenirEvento(ev);
    this.estaEncima = false;
  }

  ficherosSoltados(event) {
    this._prevenirEvento(event);
    this.estaEncima = false;
    console.log(event.dataTransfer.files);
    Array.from(event.dataTransfer.files)
      .filter((fichero: File) => {
        if (fichero.type.indexOf('image') === 0) {
          console.log('Todo en orden');
          return true;
        } else {
          console.log('Este fichero no es de tipo imagen', fichero.name);
          return false;
        }
      })
      .forEach((fichero: File) => {
        const imagen: FicheroImagen = {
          nombre: fichero.name.slice(0, -4),
          peso: fichero.size,
          fichero,
          progreso: 0
        };
        this.imagenes.push(imagen);
      });
    console.log(this.imagenes);
  }

  private _prevenirEvento(event) {
    event.preventDefault();
    event.stopPropagation();
  }

  limpiarLista() {
    this.imagenes = [];
    console.log('lista limpia');
  }

  subirFicheros() {
    this._firebaseStorage.subirImagenesAlServidor(this.imagenes);
    // console.log(this.imagenes);
  }
}
