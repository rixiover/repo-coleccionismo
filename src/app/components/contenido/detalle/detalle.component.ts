import { Component, OnInit } from '@angular/core';
import { ImagenCompleta } from 'src/app/interfaces/ImagenCompleta';
import { FirebaseService } from 'src/app/services/firebase.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {
  public carteles = [];
  public cartel = {};
  constructor(
    private service: FirebaseService,
    private _activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this._activatedRoute.params.subscribe(params =>
      this.service.cargarImagenes().subscribe(res => {
        this.carteles = res;
        console.log(params.id);
        // this.cartel = res.filter(elem => console.log(elem['id']));
        // Pendiente sacar interface para elem
        this.carteles = res.filter( (elem: any ) => {
          console.log(typeof params.id, typeof elem.id);
          return elem.id == params.id;
        });
        console.log(this.carteles);
      })
    );
  }
}
