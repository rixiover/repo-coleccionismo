import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-base-completa',
  templateUrl: './base-completa.component.html',
  styleUrls: ['./base-completa.component.css']
})
export class BaseCompletaComponent implements OnInit {
  public carteles = [];
  constructor(private _service: FirebaseService, private router: Router) {}

  ngOnInit() {
    this._service.cargarImagenes().subscribe(res => {
      this.carteles = res;
      console.log(this.carteles);
    });
  }

  // verDetalle(i: number) {
  //   this.router.navigate(['/coleccion', i]);
  // }
}
