import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-resultado-busqueda',
  templateUrl: './resultado-busqueda.component.html',
  styleUrls: ['./resultado-busqueda.component.css']
})
export class ResultadoBusquedaComponent implements OnInit {
  termino: string;
  carteles = [];
  constructor(
    private _service: FirebaseService,
    private _activatedRouted: ActivatedRoute
  ) {}

  ngOnInit() {
    this.carteles = [];
    this._activatedRouted.params.subscribe(params => {
      this.termino = params.term;
      // console.log(params.term);
      this._service.cargarImagenes().subscribe(res => {
        this.carteles = [];
        for (let i = 0; i < res.length; i++) {
          if (res[i]['nombre'].toLowerCase().indexOf(params.term.toLowerCase()) >= 0) {
            this.carteles.push(res[i]);
          }
        }
      });
    });
  }
}
