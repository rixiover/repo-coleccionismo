import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortadaComponent } from './components/portada/portada.component';
import { AboutComponent } from './components/about/about.component';
import { BaseCompletaComponent } from './components/contenido/base-completa/base-completa.component';
import { DetalleComponent } from './components/contenido/detalle/detalle.component';
import { ResultadoBusquedaComponent } from './components/contenido/resultado-busqueda/resultado-busqueda.component';
import { IndividualComponent } from './components/formularios/individual/individual.component';
import { MultipleComponent } from './components/formularios/multiple/multiple.component';
import { NuevoComponent } from './components/formularios/nuevo/nuevo.component';

const routes: Routes = [
  { path: 'portada', component: PortadaComponent },
  { path: 'about', component: AboutComponent },
  { path: 'coleccion', component: BaseCompletaComponent },
  { path: 'coleccion/:id', component: DetalleComponent },
  { path: 'resultado/:term', component: ResultadoBusquedaComponent },
  { path: 'formIndividual', component: IndividualComponent },
  { path: 'formMultiple', component: MultipleComponent },
  { path: 'nuevoRegistro', component: NuevoComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'portada' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
