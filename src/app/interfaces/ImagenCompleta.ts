export interface ImagenCompleta {
  id: number;
  url: string;
  nombre: string;
  grupo: string;
  subgrupo: string;
  fecha: Date;
  ilustrador: string;
  tipoIlustracion: string;
  valoración: number;
}
