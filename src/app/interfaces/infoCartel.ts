export interface InfoCartel {
  id: number;
  nombre: string;
  grupo: string;
  subgrupo: string;
  fecha: Date;
  ilustrador: string;
  tipoIlustración: string;
  valoración: number;
}
